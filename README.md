# IMDb Go API

[![Build Status][1]][2] [![Godoc][3]][4]

`go get gitlab.com/B4dM4n/imdb`

Bugs, comments, questions: create a [new issue][5].

Also, IMDb has [alternative interfaces][6].

[1]: https://github.com/B4dM4n/imdb/actions/workflows/build.yml/badge.svg
[2]: https://github.com/B4dM4n/imdb/actions/workflows/build.yml
[3]: https://godoc.org/gitlab.com/B4dM4n/imdb?status.png
[4]: https://godoc.org/gitlab.com/B4dM4n/imdb
[5]: https://gitlab.com/B4dM4n/imdb/issues/new
[6]: http://www.imdb.com/interfaces
